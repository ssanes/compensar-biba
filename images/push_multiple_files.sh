#!/bin/bash

#functions
get_unpushed_png (){
while read line; do
  if [[ $line =~ png$ ]] ;
  then echo $line;
fi
done < $1
}

commit_png_files (){
while read line; do
git add $line
git commit $line -m \"$line\"
done < $1
}

#main
git status > status.txt

get_unpushed_png status.txt > status_png.txt
rm status.txt

commit_png_files status_png.txt

git push
